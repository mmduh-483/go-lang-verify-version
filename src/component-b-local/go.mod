module gitlab.com/mmduh-483/go-lang-verify-version/src/component-b-local

go 1.17

require gitlab.com/mmduh-483/go-lang-verify-version/src/component-a v0.0.0-00010101000000-000000000000

replace gitlab.com/mmduh-483/go-lang-verify-version/src/component-a => ../component-a
